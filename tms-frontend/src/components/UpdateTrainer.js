import { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

import {
    Form,
    Button,
    Container,
    ButtonGroup,
    Row,
    Col
} from 'react-bootstrap';

import UpdateTrainerService from '../services/UpdateTrainerService';

export default function UpdateTrainer() {

    //Empty object data
    const initialFormData = ({
        fname: "",
        lname: "",
        email: "",
        pass: "",
        status: "",
        gender: "",
        phone: "",
        skills: "",
        qualification: "",
        experience: "",
        capacity: "100",
        address: "",
        role: "",
        company: "Cognizant",
        holding: "",
        area: "",
        city: "",
        state: "",
        country: "India",
        pin: ""
    });

    //Getting state (id) variable from Trainers
    const { state } = useLocation();
    const [selectedTrainer, setSelectedTrainers] = useState([]);
    let arrAddress = [];

    useEffect(() => {
        const timer = setTimeout(() => {
            axios.get("http://localhost:8092/tms/api/v1/getuser/" + state.id)
                .then((response) => {
                    if (response.status === 200) {
                        setSelectedTrainers(response.data)
                        // if (Object.keys(selectedTrainer).length !== 0) {
                        //     arrAddress = selectedTrainer.address.split(', ');
                        // }
                    }
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);
                    }
                });
        }, 1000);
        return () => clearTimeout(timer);
    }, [state.id]);

    if (Object.keys(selectedTrainer) != 0) {
        arrAddress = selectedTrainer.address.split(', ');
    }

    if (Object.keys(selectedTrainer) != 0 && Object.keys(arrAddress) != 0) {

    }



    // Handling the form data
    const [formData, updateFormData] = useState(initialFormData);

    // Handling the form validation
    const [validated, setValidated] = useState(false);

    const handleChange = (event) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
    };

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        // ... get form data
        event.preventDefault();

        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            console.log("Inside If")
        }
        else {
            setValidated(true);
            // ... send data to service for API
            ReactDOM.createRoot(document.getElementById('service')).render(<UpdateTrainerService data={formData} userid={state.id} />);
            console.log("Below ReactDOM");
            console.log(formData);
            // ... navigate
            // window.location = "/trainers";
        }
    }

    return (
        <>
            <Container className="form-container">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicFirstname">
                                <Form.Label className="mb-1">First Name<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="fname" defaultValue={selectedTrainer.fname} onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicLastname">
                                <Form.Label className="mb-1">Last Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="lname" defaultValue={selectedTrainer.lname} placeholder="Doe" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicEmail">
                                <Form.Label className="mb-1">Email<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="email" name="email" defaultValue={selectedTrainer.email} placeholder="johndoe@example.com" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicPhone">
                                <Form.Label className="mb-1">Phone<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="phone" defaultValue={selectedTrainer.phone} placeholder="890123456" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicGender">
                                <Form.Label className="mb-1">Gender<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Select Gender" name="gender" defaultValue={selectedTrainer.gender} onChange={handleChange} required >
                                    <option disabled>Choose gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicSkills">
                                <Form.Label className="mb-1">Skills<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="skills" defaultValue={selectedTrainer.skills} placeholder="HTML, CSS, JS, ReactJS" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicQualification">
                                <Form.Label className="mb-1">Qualification<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Select qualification" defaultValue={selectedTrainer.qualification} name="qualification" onChange={handleChange} required>
                                    <option disabled>Choose qualification</option>
                                    <option value="B.Tech">B.Tech</option>
                                    <option value="M.Tech">M.Tech</option>
                                    <option value="BCA">BCA</option>
                                    <option value="MCA">MCA</option>
                                    <option value="B.Sc">B.Sc</option>
                                    <option value="M.Sc">M.Sc</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicExperience">
                                <Form.Label className="mb-1">Years of Experience<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="number" defaultValue={selectedTrainer.experience} placeholder="10" name="experience" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>


                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressHolding">
                                <Form.Label className="mb-1">House/Flat/Holding Number<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="holding" defaultValue={arrAddress[0]} placeholder="1234" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressArea">
                                <Form.Label className="mb-1">Area<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="area" defaultValue={arrAddress[1]} placeholder="Nagawara" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressCity">
                                <Form.Label className="mb-1">City<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="city" defaultValue={arrAddress[2]} placeholder="John" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressState">
                                <Form.Label className="mb-1">State<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="state" defaultValue={arrAddress[3]} placeholder="John" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressCountry">
                                <Form.Label className="mb-1">Country<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="country" onChange={handleChange} value="India" disabled />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressPincode">
                                <Form.Label className="mb-1">Zip/Pin Code<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="pin" defaultValue={arrAddress[5]} placeholder="666666" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block" type="submit">Update Trainer</Button>
                        <div id="service" />
                    </ButtonGroup>
                </Form>
            </Container>
        </>
    );
}
