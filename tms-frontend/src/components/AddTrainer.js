import { useState } from 'react';
import ReactDOM from 'react-dom/client';

import {
    Form,
    Button,
    Container,
    ButtonGroup,
    Row,
    Col
} from 'react-bootstrap';

import AddTrainerService from '../services/AddTrainerService';
import { useNavigate } from 'react-router-dom';

export default function AddTrainer() {
    const navigate = useNavigate();

    const baseURL = "http://localhost:8092/tms/api/v1/adduser";

    //Empty object data
    const initialFormData = Object.freeze({
        fname: "",
        lname: "",
        email: "",
        pass: "",
        status: "Pending",
        gender: "",
        phone: "",
        skills: "",
        qualification: "",
        experience: "",
        capacity: "90",
        address: "",
        role: "Trainer",
        company: "Cognizant",
        holding: "",
        area: "",
        city: "",
        state: "",
        country: "India",
        pin: ""
    });

    // Handling the form data
    const [formData, updateFormData] = useState(initialFormData);
    const [validated, setValidated] = useState(false);

    const handleChange = (event) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
    };

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        // ... get form data
        event.preventDefault();
        console.log(formData);

        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            console.log("Inside If")
        }
        else {
            setValidated(true);
            // ... send data to service for API
            ReactDOM.createRoot(document.getElementById('service')).render(<AddTrainerService data={formData} />);

            // ... navigate
            window.location = "/a/trainers";
        }
    }

    return (
        <>
            <Container className="form-container">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicFirstname">
                                <Form.Label className="mb-1">First Name<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="fname" placeholder="John" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicLastname">
                                <Form.Label className="mb-1">Last Name<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="lname" placeholder="Doe" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicEmail">
                                <Form.Label className="mb-1">Email<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="email" name="email" placeholder="johndoe@example.com" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicPhone">
                                <Form.Label className="mb-1">Phone<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="phone" placeholder="890123456" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicGender">
                                <Form.Label className="mb-1">Gender<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Select Gender" name="gender" defaultValue="Choose gender" onChange={handleChange} required >
                                    <option disabled>Choose gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicSkills">
                                <Form.Label className="mb-1">Skills<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="skills" placeholder="HTML, CSS, JS, ReactJS" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicQualification">
                                <Form.Label className="mb-1">Qualification<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Select qualification" name="qualification" defaultValue="Choose qualification" onChange={handleChange} required>
                                    <option disabled>Choose qualification</option>
                                    <option value="B.Tech">B.Tech</option>
                                    <option value="M.Tech">M.Tech</option>
                                    <option value="BCA">BCA</option>
                                    <option value="MCA">MCA</option>
                                    <option value="B.Sc">B.Sc</option>
                                    <option value="M.Sc">M.Sc</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicExperience">
                                <Form.Label className="mb-1">Years of Experience<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="number" placeholder="10" name="experience" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>


                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressHolding">
                                <Form.Label className="mb-1">House/Flat/Holding Number<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="holding" placeholder="1234" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressArea">
                                <Form.Label className="mb-1">Area<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="area" placeholder="Nagawara" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressCity">
                                <Form.Label className="mb-1">City<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="city" placeholder="John" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressState">
                                <Form.Label className="mb-1">State<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="state" placeholder="John" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressCountry">
                                <Form.Label className="mb-1">Country<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="country" onChange={handleChange} value="India" disabled />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicAddressPin">
                                <Form.Label className="mb-1">Zip/Pin Code<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="pin" placeholder="666666" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block" type="submit">Add Trainer</Button>
                        <div id="service" />
                    </ButtonGroup>
                </Form>
            </Container>
        </>
    );
}