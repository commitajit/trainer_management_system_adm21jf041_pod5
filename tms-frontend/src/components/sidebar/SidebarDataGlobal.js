import * as Icon from 'react-icons/fi/';

export const SidebarDataGlobal = [
    {
        global: [
            {
                title: "Logout",
                path: "/login",
                icon: <Icon.FiFile />
            }
        ]
    }
]