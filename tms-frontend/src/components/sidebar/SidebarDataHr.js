import * as Icon from 'react-icons/fi/';

export const SidebarDataHr = [
    {
        trainers: [
            {
                title: "Trainers",
                path: "/h/trainers",
                icon: <Icon.FiFile />
            },
            {
                title: "Logout",
                path: "/logout",
                icon: <Icon.FiLogOut />
            }
        ]
    }
]