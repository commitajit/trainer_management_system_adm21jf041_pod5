import {
    Row,
    Col,
    Image
} from 'react-bootstrap';
import { SidebarDataManager } from "./SidebarDataManager";
import { Link } from 'react-router-dom';
import logo from "../../assets/images/tms_logo_variant_light.png";

export default function SidebarM() {
    return (
        <>
            {/* <Container fluid className="m-0 p-0"> */}
            <Col className="sidebar m-0 px-3 py-3 vh-100">
                {/* Logo */}
                <Image
                    src={logo}
                    width="100%"
                    height="auto"
                    className="text-center"
                    alt="tms_logo"
                />
                <hr className="my-4" />

                {/* Sidebar Menus */}
                <Row>
                    <div className='sidebar-main-menu'>
                        {/* First Menu */}
                        <ul className='mb-4'>
                            <span className='menu-title text-muted'>COHORT</span>
                            {
                                SidebarDataManager.map(
                                    (obj) => (
                                        obj.cohorts.map(
                                            (item, index) => (
                                                <li className='sidebar-menu-item' key={index}>
                                                    <Link to={item.path}>
                                                        <div className='menu-item-single'>
                                                            {item.icon} {item.title}
                                                        </div>
                                                    </Link>
                                                </li>
                                            )
                                        )
                                    )
                                )
                            }
                        </ul>
                    </div>
                </Row>
            </Col>
            {/* </Container> */}
        </>
    )
}