import * as Icon from 'react-icons/fi/';

export const SidebarDataAdmin = [
    {
        trainers: [
            {
                title: "Trainers",
                path: "/a/trainers",
                icon: <Icon.FiFile />
            },

            {
                title: "Add Trainer",
                path: "/a/trainers/add",
                icon: <Icon.FiFilePlus />
            }
        ],

        teams: [
            {
                title: "Team",
                path: "/a/team",
                icon: <Icon.FiUsers />
            },

            {
                title: "Add Member",
                path: "/a/team/add",
                icon: <Icon.FiUserPlus />
            },

            {
                title: "Logout",
                path: "/logout",
                icon: <Icon.FiLogOut />
            }
        ]
    }
]