import * as Icon from 'react-icons/fi/';

export const SidebarDataManager = [
    {
        cohorts: [
            {
                title: "Unallocated Cohorts",
                path: "/m/cohorts/unallocated",
                icon: <Icon.FiFile />
            },
            {
                title: "Allocated Cohorts",
                path: "/m/cohorts/allocated",
                icon: <Icon.FiFile />
            },

            {
                title: "Add Cohorts",
                path: "/m/cohorts/add",
                icon: <Icon.FiFilePlus />
            },
            {
                title: "Logout",
                path: "/logout",
                icon: <Icon.FiLogOut />
            }
        ]
    }
]