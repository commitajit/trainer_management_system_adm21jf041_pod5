import { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

import {
    Form,
    Button,
    Container,
    ButtonGroup,
    Row,
    Col
} from 'react-bootstrap';

import AllocateCohortService from '../services/AllocateCohortService';

export default function AllocateCohort() {

    //Empty object data
    const initialFormData = ({
        code: "",
        strength: "",
        domain: "",
        userId: ""
    });

    let newCapacity = 0;

    //Getting state (id) variable from Trainers
    const { state } = useLocation();
    const [selectedCohort, setSelectedCohort] = useState([]);
    const [availableTrainers, setAvailableTrainers] = useState([]);

    useEffect(() => {
        const timer = setTimeout(() => {
            axios.get("http://localhost:8093/tms/api/v1/cohortsdetails/" + state.code)
                .then((response) => {
                    if (response.status === 200) {
                        console.log(response.data);
                        setSelectedCohort(response.data)
                    }
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);
                    }
                });

            axios.get("http://localhost:8092/tms/api/v1/availabletrainers/" + state.code)
                .then((response) => {
                    if (response.status === 200) {
                        console.log(response.data);
                        setAvailableTrainers(response.data);
                    }
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);
                    }
                });
        }, 1000);
        return () => clearTimeout(timer);
    }, [state.code]);

    // Handling the form data
    const [formData, updateFormData] = useState(initialFormData);

    // Handling the form validation
    const [validated, setValidated] = useState(false);

    const handleChange = (event) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
    };

    const handleSubmit = (event,) => {
        const form = event.currentTarget;
        // ... get form data
        event.preventDefault();

        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            setValidated(true);
            // ... send data to service for API
            ReactDOM.createRoot(document.getElementById('service')).render(<AllocateCohortService data={formData} cohortcode={state.code} />);
        }
    }

    return (
        <>
            <Container className="form-container">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicCode">
                                <Form.Label className="mb-1">Cohort Code<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="code" defaultValue={selectedCohort.code} placeholder="ADM00JF000" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicStrength">
                                <Form.Label className="mb-1">Strength<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="strength" defaultValue={selectedCohort.strength} placeholder="30" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicDomain">
                                <Form.Label className="mb-1">Domain<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="domain" defaultValue={selectedCohort.domain} placeholder="ADM" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicTrainer">
                                <Form.Label className="mb-1">Select trainer<span className="text-danger">*</span></Form.Label>
                                <Form.Select aria-label="Select trainer" name="userId" defaultValue="Choose trainer" onChange={handleChange} required >
                                    <option disabled>Choose trainer</option>
                                    {
                                        availableTrainers.map((x, index) => {

                                            return (
                                                <option value={x.id} key={index}>{x.fname} {x.lname} {"("}{x.email}{")"}</option>
                                            )
                                        })
                                    }
                                </Form.Select>
                            </Form.Group>
                        </Col>
                    </Row>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block" type="submit">Allocate Cohort</Button>
                        <div id="service" />
                    </ButtonGroup>
                </Form>
            </Container>
        </>
    );
}
