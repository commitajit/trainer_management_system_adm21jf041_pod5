import { useState } from 'react';
import ReactDOM from 'react-dom/client';

import {
    Form,
    Button,
    Container,
    Image,
    ButtonGroup,
    Stack
} from 'react-bootstrap';

import logo from "../assets/images/tms_logo_variant_light.png";
import RegistrationService from '../services/RegistrationService';
import { useNavigate, Link } from 'react-router-dom';

export default function RegistrationForm() {

    const navigate = useNavigate();

    //Empty object data
    const initialFormData = Object.freeze({
        email: "",
        pass: "",
        company: "",
        role: "Admin"
    });

    // Handling the form data
    const [formData, updateFormData] = useState(initialFormData);



    const handleChange = (e) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [e.target.name]: e.target.value.trim()
        });
    };

    const handleSubmit = (e) => {
        // ... get form data
        e.preventDefault();
        // console.log(formData);

        // ... send data to service for API
        ReactDOM.createRoot(document.getElementById('service')).render(<RegistrationService data={formData} />);


        // ... navigate to login page
        navigate("/login");

    }

    return (
        <>
            <Container className="form-container">
                <Container className="text-center">
                    <Image
                        src={logo}
                        width="auto"
                        height="40"
                        className="text-center mb-5"
                        alt="tms_logo"
                    />
                </Container>

                <Stack direction="horizontal" className='mb-4'>
                    <h4><strong>Register</strong></h4>
                    <span className="ms-auto " >or <Link to="/login" className='text-decoration-none '>Log In</Link></span>
                </Stack>

                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="mb-1">Email<span className="text-danger">*</span></Form.Label>
                        <Form.Control type="email" name="email" placeholder="johndoe@example.com" onChange={handleChange} />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="mb-1">Password<span className="text-danger">*</span></Form.Label>
                        <Form.Control type="password" name="pass" placeholder="********" onChange={handleChange} />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicCompany">
                        <Form.Label className="mb-1">Company<span className="text-danger">*</span></Form.Label>
                        <Form.Select aria-label="Select Company" name="company" onChange={handleChange} >
                            <option>Choose your company</option>
                            <option value="Cognizant">Cognizant</option>
                        </Form.Select>
                    </Form.Group>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block" type="submit" onClick={handleSubmit}>Register</Button>
                        <div id="service" />
                    </ButtonGroup>
                </Form>
            </Container>
        </>
    );
}