import { Navbar, Container, Nav } from 'react-bootstrap';
import logo from "../assets/images/tms_logo_variant_dark.png";

export default function Header() {
    return (
        <Navbar className='bg-brand-darker d-flex justify-content-between' variant='dark' expand="md">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        src={logo}
                        width="auto"
                        height="40"
                        className="align-top"
                        alt="tms_logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarMenus" />
                <Navbar.Collapse id="navbarMenus" className='justify-content-end text-center'>
                    <Nav>
                        <Nav.Link href="#home">Trainer</Nav.Link>
                        <Nav.Link href="#features">Teams</Nav.Link>
                        <Nav.Link href="#pricing"></Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}