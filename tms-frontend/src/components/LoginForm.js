import { useState } from 'react';
import ReactDOM from 'react-dom/client';
import {
    Form,
    Button,
    Container,
    Image,
    ButtonGroup,
    Stack

} from 'react-bootstrap';
import { Link } from 'react-router-dom';

import LoginService from '../services/LoginService';

import logo from "../assets/images/tms_logo_variant_light.png";

export default function LoginForm() {

    const initialFormData = Object.freeze({
        email: "",
        pass: ""
    });

    const [formData, updateFormData] = useState(initialFormData);

    const handleChange = (e) => {
        updateFormData({
            // ... empty data comming from initialFormData
            ...formData,

            // Overriding the empty data with changed value and trimming any whitespace
            [e.target.name]: e.target.value.trim()
        });
    };

    const handleSubmit = async (e) => {
        // ... get form data
        e.preventDefault();
        await ReactDOM.createRoot(document.getElementById('service')).render(<LoginService data={formData} />);
    }


    return (

        <>
            <Container className="form-container">
                <Container className="text-center">
                    <Image
                        src={logo}
                        width="auto"
                        height="40"
                        className="text-center mb-5"
                        alt="tms_logo"
                    />
                </Container>

                <Stack direction="horizontal" className='mb-4'>
                    <h4><strong>Log In</strong></h4>
                    <span className="ms-auto " >or <Link to="/register" className='text-decoration-none '>create account</Link></span>
                </Stack>

                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="mb-1">Email address <span className="text-danger">*</span></Form.Label>
                        <Form.Control type="email" name="email" placeholder="Enter email" onChange={handleChange} />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="mb-1">Password <span className="text-danger">*</span></Form.Label>
                        <Form.Control type="password" name="pass" placeholder="Password" onChange={handleChange} />
                    </Form.Group>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block bg-pink" type="submit" onClick={handleSubmit}>
                            Sign in
                        </Button>
                        <div id="service" />
                    </ButtonGroup>

                </Form>
            </Container>

        </>
    );
}