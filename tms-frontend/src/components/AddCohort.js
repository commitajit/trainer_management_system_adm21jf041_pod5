import { useState } from 'react';
import ReactDOM from 'react-dom/client';

import {
    Form,
    Button,
    Container,
    ButtonGroup,
    Row,
    Col
} from 'react-bootstrap';

import AddCohortService from '../services/AddCohortService';

export default function AddCohort() {

    //Empty object data
    const initialFormData = Object.freeze({
        code: "",
        strength: "",
        domain: "",
        user: ""
    });

    // Handling the form data
    const [formData, updateFormData] = useState(initialFormData);
    const [validated, setValidated] = useState(false);

    const handleChange = (event) => {
        updateFormData({
            ...formData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
    };

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        // ... get form data
        event.preventDefault();
        console.log(formData);

        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            setValidated(true);
            // ... send data to service for API
            ReactDOM.createRoot(document.getElementById('service')).render(<AddCohortService data={formData} />);
            // ... navigate
            window.location = "/m/cohorts/unallocated";
        }
    }

    return (
        <>
            <Container className="form-container">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicCode">
                                <Form.Label className="mb-1">Cohort Code<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="code" placeholder="ADM00JF000" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicStrength">
                                <Form.Label className="mb-1">Strength<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="strength" placeholder="30" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group className="mb-4" controlId="formBasicDomain">
                                <Form.Label className="mb-1">Domain<span className="text-danger">*</span></Form.Label>
                                <Form.Control type="text" name="domain" placeholder="ADM" onChange={handleChange} required />
                            </Form.Group>
                        </Col>
                    </Row>

                    <ButtonGroup className="d-flex">
                        <Button variant="primary" className="btn-block" type="submit">Add Cohort</Button>
                        <div id="service" />
                    </ButtonGroup>
                </Form>
            </Container>
        </>
    );
}