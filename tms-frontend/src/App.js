import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import UserRegistration from './pages/UserRegistration';
import UserLogin from "./pages/UserLogin";
import TrainersA from "./pages/TrainersA";
import TrainersH from "./pages/TrainersH";
import UnallocatedCohorts from "./pages/UnallocatedCohorts";
import AllocatedCohorts from "./pages/AllocatedCohorts";
import Team from "./pages/Team";
import Logout from "./pages/Logout";
import AddTrainer from "./components/AddTrainer";
import AddMember from "./components/AddMember";
import AddCohort from "./components/AddCohort";
import AllocateCohort from "./components/AllocateCohort";
import UpdateTrainer from "./components/UpdateTrainer";
import UpdateMember from "./components/UpdateMember";

function App() {
  return (
    <>
      <Router>
        <Routes>
          {/* GLOBAL */}
          <Route path="/" element={<UserLogin />} exact />
          <Route index path="/login" element={<UserLogin />} />
          <Route path="/register" element={<UserRegistration />} />
          <Route path="/logout" element={<Logout />} />

          {/* FOR ADMIN */}
          <Route path="/a/dashboard" element={<TrainersA />} />
          <Route path="/a/trainers" element={<TrainersA />} />
          <Route path="/a/trainers/add" element={<AddTrainer />} />
          <Route path="/a/trainers/update" element={<UpdateTrainer />} />

          <Route path="/a/team" element={<Team />} />
          <Route path="/a/team/add" element={<AddMember />} />
          <Route path="/a/team/update" element={<UpdateMember />} />

          {/* FOR HR */}
          <Route path="/h/dashboard" element={<TrainersH />} />
          <Route path="/h/trainers" element={<TrainersH />} />

          {/* FOR MANAGER */}
          <Route path="/m/dashboard" element={<UnallocatedCohorts />} />
          <Route path="/m/cohorts/unallocated" element={<UnallocatedCohorts />} />
          <Route path="/m/cohorts/allocated" element={<AllocatedCohorts />} />
          <Route path="/m/Cohorts/add" element={<AddCohort />} />
          <Route path="/m/Cohorts/allocate" element={<AllocateCohort />} />

        </Routes>
      </Router>
    </>

  );
}

export default App;
