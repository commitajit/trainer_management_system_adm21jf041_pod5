import { Col, Row, Button, Stack, Table } from 'react-bootstrap';
import SidebarH from '../components/sidebar/SidebarH'
import { useState, useEffect } from "react";
import axios from 'axios';


export default function TrainersH() {

    const [pendingTrainers, setPendingTrainers] = useState([]);
    const [approvedTrainers, setApprovedTrainers] = useState([]);

    const getPendingTrainersBaseUrl = "http://localhost:8092/tms/api/v1/pendingtrainers";
    const getApprovedTrainersBaseUrl = "http://localhost:8092/tms/api/v1/approvedtrainers";
    const updateUserStatusBaseUrl = "http://localhost:8092/tms/api/v1/user/";

    useEffect(() => {
        axios.get(getPendingTrainersBaseUrl).then((responseFirst) => {
            setPendingTrainers(responseFirst.data);
        });
        axios.get(getApprovedTrainersBaseUrl).then((responseSecond) => {
            setApprovedTrainers(responseSecond.data);
        });
    }, []);


    // Approve the user
    const approveTrainer = async (id, email) => {
        let response = window.confirm(
            "Are you sure to approve the trainer with the email " + email + "?"
        );

        if (response) {
            response = await axios.put(
                updateUserStatusBaseUrl + id + "/" + "Approved"
            );

            response = await axios.get(
                getApprovedTrainersBaseUrl
            )
            setApprovedTrainers(response.data);

            response = await axios.get(
                getPendingTrainersBaseUrl
            );
            setPendingTrainers(response.data);
        }
    }

    // Unapprove the user
    const unApproveTrainer = async (id, email) => {
        let response = window.confirm(
            "Are you sure to unapprove the trainer with the email " + email + "?"
        );

        if (response) {
            response = await axios.put(
                updateUserStatusBaseUrl + id + "/" + "Pending"
            );
            response = await axios.get(
                getApprovedTrainersBaseUrl
            )
            setApprovedTrainers(response.data);

            response = await axios.get(
                getPendingTrainersBaseUrl
            );
            setPendingTrainers(response.data);

        }
    }

    // Reject the user
    const rejectTrainer = async (id, email) => {
        let response = window.confirm(
            "Are you sure to reject the trainer with the email " + email + "?"
        );

        if (response) {
            response = await axios.put(
                updateUserStatusBaseUrl + id + "/" + "Rejected"
            );

            response = await axios.get(
                getApprovedTrainersBaseUrl
            )
            setApprovedTrainers(response.data);

            response = await axios.get(
                getPendingTrainersBaseUrl
            );
            setPendingTrainers(response.data);
        }
    }



    return (
        <>

            <div className='d-flex'>
                <SidebarH />
                <Col className='content-container p-4'>
                    <Row className='m-0 p-0 mb-4'>
                        <Stack direction="horizontal" gap={3}>
                            <h2 className='fw-bold'>Trainers</h2>
                        </Stack>
                    </Row>

                    <div className='table-container'>
                        <Table responsive striped hover className='m-0 p-0'>
                            <thead>
                                <tr>
                                    <th style={{ width: 40 }} ></th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th>Skills</th>
                                    <th>Qualification</th>
                                    <th>Experience</th>
                                    <th style={{ width: 160 }}>Address</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                {
                                    pendingTrainers.map((x, index) => (
                                        <tr key={index}>
                                            <td className='text-center'></td>
                                            <td>{x.fname} {x.lname}</td>
                                            <td>{x.email}</td>
                                            <td>{x.phone}</td>
                                            <td>{x.gender}</td>
                                            <td>{x.skills}</td>
                                            <td>{x.qualification}</td>
                                            <td>{x.experience}</td>
                                            <td size="sm">{x.address}</td>
                                            <td style={{ width: 200 }}>
                                                <Button variant="success" size="sm" id="updateBtn" onClick={() => approveTrainer(x.id, x.email)}>Approve</Button>{"  "}
                                                <Button variant="danger" size="sm" onClick={() => rejectTrainer(x.id, x.email)}>Reject</Button>
                                            </td>
                                        </tr>
                                    ))
                                }

                                {
                                    approvedTrainers.map((x, index) => (
                                        <tr key={index}>
                                            <td className='text-center'></td>
                                            <td>{x.fname} {x.lname}</td>
                                            <td>{x.email}</td>
                                            <td>{x.phone}</td>
                                            <td>{x.gender}</td>
                                            <td>{x.skills}</td>
                                            <td>{x.qualification}</td>
                                            <td>{x.experience}</td>
                                            <td size="sm">{x.address}</td>
                                            <td className=''>
                                                <Button variant="secondary" size="sm" id="updateBtn" onClick={() => unApproveTrainer(x.id, x.email)}>Unapprove</Button>{"  "}
                                                <Button variant="danger" size="sm" onClick={() => rejectTrainer(x.id, x.email)}>Reject</Button>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody >
                        </Table>
                    </div>
                </Col>
            </div>


        </>
    )
}