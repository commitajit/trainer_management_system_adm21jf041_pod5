import { Col, Row, Button, Stack, Table } from 'react-bootstrap';
import SidebarM from '../components/sidebar/SidebarM'
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';


export default function UnallocatedCohorts() {

    const navigate = useNavigate();
    const [isAddNew, setIsAddNew] = useState(false);
    const [cohorts, setCohorts] = useState([]);
    const [assignedTrainer, setAssignedTrainer] = useState([]);
    let cohortDetails = [];



    if (isAddNew === true) {
        navigate("/m/cohorts/add");
    }

    const baseURL = "http://localhost:8093/tms/api/v1/unallocatedcohorts";

    useEffect(() => {
        axios.get(baseURL).then((response) => {
            setCohorts(response.data);
        });
    }, []);

    useEffect((id) => {
        // getAssignedTrainer = async (id) => {
        let response
        if (id != null) {
            response = axios.get(
                "http://localhost:8092/tms/api/v1/getuser/" + id
            );
            setAssignedTrainer(response.data);
        }
        // }
    }, [])

    console.log(cohorts);
    console.log(assignedTrainer);

    // Delete
    const deleteCohort = async (cohortCode) => {
        let response = window.confirm(
            "Are you sure to delete the cohort " + cohortCode + "?"
        );

        if (response) {
            response = await axios.delete(
                "http://localhost:8093/tms/api/v1/cohortsdetails/" + cohortCode
            );
            response = await axios.get(
                baseURL
            );

            setCohorts(response.data);
        }
    }

    const allocateCohort = async (code) => {
        // ... navigating with sending states
        navigate('/m/cohorts/allocate', { state: { code } });
    }

    return (
        <>

            <div className='d-flex'>
                <SidebarM />
                <Col className='content-container p-4'>
                    <Row className='m-0 p-0 mb-4'>
                        <Stack direction="horizontal" gap={3}>
                            <h2 className='fw-bold'>Unallocated Cohorts</h2>
                            <span><Button variant="outline-primary" onClick={() => setIsAddNew(true)}>Add Cohort</Button></span>
                        </Stack>
                    </Row>

                    <div className='table-container'>
                        <Table responsive striped hover className='m-0 p-0'>
                            <thead>
                                <tr>
                                    <th style={{ width: 40 }}></th>
                                    <th>Code</th>
                                    <th>Strength</th>
                                    <th>Domain</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    cohorts.map((x, index) => {
                                        return (
                                            < tr key={index} >
                                                <td className='text-center'>{index + 1}</td>
                                                <td>{x.code}</td>
                                                <td>{x.strength}</td>
                                                <td>{x.domain}</td>
                                                <td className=''>
                                                    <Button variant="info" size="sm" id="updateBtn" onClick={() => allocateCohort(x.code)}>Allocate</Button>{"  "}
                                                    <Button variant="danger" size="sm" onClick={() => deleteCohort(x.code)}>Delete</Button>
                                                    <div id="assignTrainerService" />
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody >
                        </Table>
                    </div>
                </Col>
            </div >


        </>
    )
}