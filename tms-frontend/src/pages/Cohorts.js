import { Col, Row, Button, Stack, Table } from 'react-bootstrap';
import SidebarM from '../components/sidebar/SidebarM'
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';


export default function Cohorts() {

    const navigate = useNavigate();
    const [isAddNew, setIsAddNew] = useState(false);
    const [approvedTrainers, setApprovedTrainers] = useState([]);



    if (isAddNew === true) {
        navigate("a/cohorts/add");
    }

    const baseURL = "http://localhost:8092/tms/api/v1/approvedtrainers";

    useEffect(() => {
        axios.get(baseURL).then((response) => {
            setApprovedTrainers(response.data);
        });
    }, []);


    // Delete
    const deleteTrainer = async (id, email) => {
        let response = window.confirm(
            "Are you sure to delete the trainer with the email " + email + "?"
        );

        if (response) {
            response = await axios.delete(
                "http://localhost:8092/tms/api/v1/user/" + id
            );
            response = await axios.get(
                baseURL
            );

            setApprovedTrainers(response.data);
        }
    }

    const updateTrainer = async (id) => {
        // ... navigating with sending states
        navigate('/trainers/update', { state: { id } });
    }

    return (
        <>

            <div className='d-flex'>
                <SidebarM />
                <Col className='content-container p-4'>
                    <Row className='m-0 p-0 mb-4'>
                        <Stack direction="horizontal" gap={3}>
                            <h2 className='fw-bold'>Trainers</h2>
                            <span><Button variant="outline-primary" onClick={() => setIsAddNew(true)}>Add Trainer</Button></span>
                        </Stack>
                    </Row>

                    <div className='table-container'>
                        <Table responsive striped hover className='m-0 p-0'>
                            <thead>
                                <tr>
                                    <th style={{ width: 40 }} ></th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th>Skills</th>
                                    <th>Qualification</th>
                                    <th>Experience</th>
                                    <th style={{ width: 160 }}>Address</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                {
                                    approvedTrainers.map((x, index) => (
                                        <tr key={index}>
                                            <td className='text-center'>{index + 1}</td>
                                            <td>{x.fname} {x.lname}</td>
                                            <td>{x.email}</td>
                                            <td>{x.phone}</td>
                                            <td>{x.gender}</td>
                                            <td>{x.skills}</td>
                                            <td>{x.qualification}</td>
                                            <td>{x.experience}</td>
                                            <td size="sm">{x.address}</td>
                                            <td className=''>
                                                <Button variant="warning" size="sm" id="updateBtn" onClick={() => updateTrainer(x.id)}>Update</Button>{"  "}
                                                <Button variant="danger" size="sm" onClick={() => deleteTrainer(x.id, x.email)}>Delete</Button>

                                                <div id="updateService" />
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody >
                        </Table>
                    </div>
                </Col>
            </div>


        </>
    )
}