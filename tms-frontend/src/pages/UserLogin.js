import LoginForm from '../components/LoginForm';
import { Container, Row, Col } from 'react-bootstrap';

export default function UserLogin() {
    return (
        <>
            <Container fluid className="bg-image-full d-flex">
                <Container className='d-flex justify-content-center'>
                    <Col xl={4} className="align-self-center">
                        <LoginForm />
                    </Col>
                </Container>
            </Container>
        </>
    )
}