import RegistrationForm from '../components/RegistrationForm';
import { Container, Col } from 'react-bootstrap';

export default function UserLogin() {
    return (
        <>
            <Container fluid className="bg-image-full d-flex">
                <Container className='d-flex justify-content-center'>
                    <Col xl={4} className="mt-5 align-self-top">
                        <RegistrationForm />
                    </Col>
                </Container>
            </Container>
        </>
    )
}