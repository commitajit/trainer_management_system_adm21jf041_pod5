import { Col, Row, Button, Stack, Table } from 'react-bootstrap';
import SidebarM from '../components/sidebar/SidebarM'
import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';


export default function AllocatedCohorts() {

    const navigate = useNavigate();
    const [isAddNew, setIsAddNew] = useState(false);
    const [cohortDetails, setCohortDetails] = useState([]);
    let cohort = [];


    if (isAddNew === true) {
        navigate("/m/cohorts/add");
    }

    const baseURL = "http://localhost:8093/tms/api/v1/allocatedcohorts";

    useEffect(() => {
        axios.get(baseURL).then((response) => {
            if (response.status === 200) {
                setCohortDetails(response.data);
            }
        });

    }, []);

    const unallocateCohort = async (code) => {
        let response = window.confirm(
            "Are you sure to unallocate the trainer to the cohort " + code + "?"
        );

        if (response) {
            response = await axios.put(
                "http://localhost:8093/tms/api/v1/unallocatecohort/" + code
            );
            response = await axios.get(
                baseURL
            );

            setCohortDetails(response.data);
        }
    }

    return (
        <>

            <div className='d-flex'>
                <SidebarM />
                <Col className='content-container p-4'>
                    <Row className='m-0 p-0 mb-4'>
                        <Stack direction="horizontal" gap={3}>
                            <h2 className='fw-bold'>Allocated Cohorts</h2>
                        </Stack>
                    </Row>

                    <div className='table-container'>
                        <Table responsive striped hover className='m-0 p-0'>
                            <thead>
                                <tr>
                                    <th style={{ width: 40 }}></th>
                                    <th>Code</th>
                                    <th>Strength</th>
                                    <th>Domain</th>
                                    <th>Assigned Trainer</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    cohortDetails.map((x, index) => {
                                        cohort = x.split(",");
                                        return (
                                            < tr key={index} >
                                                <td className='text-center'>{index + 1}</td>
                                                <td>{cohort[0]}</td>
                                                <td>{cohort[1]}</td>
                                                <td>{cohort[2]}</td>
                                                <td>{cohort[4]} {cohort[5]} {"("}{cohort[6]}{")"}</td>
                                                <td className=''>
                                                    <Button variant="secondary" size="sm" id="updateBtn" onClick={() => unallocateCohort(cohort[0])}>Unllocate</Button>{"  "}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody >
                        </Table>
                    </div>
                </Col>
            </div >


        </>
    )
}