import axios from "axios";
import { useState } from "react";

export default function LoginService(props) {


    axios.post('http://localhost:8091/tms/api/v1/login', props.data)
        .then((response) => {
            if (response.status === 200) {
                console.log(response);
                axios.get('http://localhost:8092/tms/api/v1/user/' + props.data.email)
                    .then((result) => {
                        const userRole = result.data.role[0].role;

                        if (userRole === "Admin" || userRole === "admin" || userRole === "ADMIN") {
                            window.location = "/a/dashboard";
                        }
                        else if (userRole === "Hr" || userRole === "hr" || userRole === "HR") {
                            window.location = "/h/dashboard";
                        }
                        else if (userRole === "Manager" || userRole === "manager" || userRole === "MANAGER") {
                            window.location = "/m/dashboard";
                        }
                    })
            }
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        });
}

