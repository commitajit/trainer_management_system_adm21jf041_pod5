import axios from "axios";
import RegistrationForm from "../components/RegistrationForm";


export default function RegistrationService(props) {

    axios.post('http://localhost:8091/tms/api/v1/register', props.data)
        .then(function (data) {
            <RegistrationForm data={data.response.data} />
            console.log(data.response.data);
            console.log(data.response.status);
            console.log(data.response.headers);

            if (data.response.data.message === "Error: Email is already taken!") {
                console.log("SUCCESS")
            }
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        });



}