import axios from "axios";


export default function UpdateTrainerService(props) {

    axios.put('http://localhost:8092/tms/api/v1/user/' + props.userid, props.data)
        .then(function (response) {
            if (response.status === 200) {
                window.location = "/a/trainers"
            }
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        });

}