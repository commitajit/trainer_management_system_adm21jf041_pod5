import React, { Component } from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';

class PendingTrainers extends Component {

    state = {
        pendingtrainers: []
    };

    componentDidMount = async () => {
        const response = axios.get(
            "http://localhost:8092/tms/api/v1/pendingtrainers"
        );

        this.setState({
            pendingtrainers: response.data
        });
    }

    // deleteEmployee = async (id) => {

    //     let response = window.confirm(
    //         "Do you really want to delete the trainer with the Id : " + id + "?"
    //     );

    //     if (response) {
    //         response = axios.delete(
    //             "http://localhost:8092/tms/api/v1/user/" + id
    //         );
    //         response = axios.get(
    //             "http://localhost:8092/tms/api/v1/pendingtrainers"
    //         );

    //         this.setState({
    //             pendingtrainers: response.data
    //         });
    //     }
    // }

    render() {
        return (
            <>
                <table className="table table-striped table-bordered">
                    <tbody>
                        {
                            this.state.pendingtrainers.map((x) => (
                                <tr key={x.id}>
                                    <td>{x.id}</td>
                                    <td>{x.fname}</td>
                                    <td>{x.lname}</td>
                                    <td>{x.email}</td>
                                    <td>{x.phone}</td>
                                    <td>{x.gender}</td>
                                    <td>{x.skills}</td>
                                    <td>{x.qualification}</td>
                                    <td>{x.experience}</td>
                                    <td>{x.address}</td>
                                    <td><Button onClick={() => this.deleteEmployee(x.id)}>Delete</Button></td>
                                </tr>
                            ))
                        }
                    </tbody >
                </table>

            </>

        );
    }

}

export default PendingTrainers;