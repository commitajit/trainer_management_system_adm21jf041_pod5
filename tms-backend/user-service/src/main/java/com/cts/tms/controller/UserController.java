package com.cts.tms.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CookieValue;
//import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.tms.exception.UserNotFoundException;
import com.cts.tms.feign.AuthorizationClient;
import com.cts.tms.model.ERole;
import com.cts.tms.model.RoleModel;
import com.cts.tms.model.UserModel;
import com.cts.tms.payloadrequest.UserRequest;
import com.cts.tms.repository.RoleRepository;
import com.cts.tms.service.UserService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/tms/api/v1/")
@Slf4j
public class UserController {

	@Autowired
	private AuthorizationClient authorizationClient;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/approvedtrainers")
	@ApiOperation(value = "View approved trainers", notes = "Get list of approved trainers", httpMethod = "GET", response = List.class)
	public ResponseEntity<List<UserModel>> viewApprovedTrainers() {
		log.info("Inside approved trainer function");
		return new ResponseEntity<List<UserModel>>(userService.viewApprovedTrainer(), HttpStatus.OK);
	}

	@GetMapping("/pendingtrainers")
	@ApiOperation(value = "View pending trainers", notes = "Get list of pending trainers", httpMethod = "GET", response = List.class)
	public ResponseEntity<List<UserModel>> viewPendingTrainers() {
		log.info("Inside pending trainer function");
		return new ResponseEntity<List<UserModel>>(userService.viewPendingTrainer(), HttpStatus.OK);
	}

	@GetMapping("/getuser/{id}")
	@ApiOperation(value = "View user details by id", notes = "Get the details of user by his id", httpMethod = "GET", response = List.class)
	public ResponseEntity<UserModel> getUserById(@PathVariable(name = "id") int id) {
		log.info("Inside getuser function");
		return new ResponseEntity<UserModel>(userService.viewUserById(id), HttpStatus.OK);
	}
	
	

	@GetMapping("/user/{email}")
	@ApiOperation(value = "View user details by email", notes = "Get the details of user by his email", httpMethod = "GET", response = List.class)
	public ResponseEntity<UserModel> getUserByEmail(@PathVariable(name = "email") String email) {
		log.info("Inside getuser function");
		UserModel user = new UserModel();
		user = userService.viewUserByEmail(email);
		if (user == null)
			throw new UserNotFoundException("User Not Found");
		return new ResponseEntity<UserModel>(userService.viewUserByEmail(email), HttpStatus.OK);
	}

	@GetMapping("/team")
	@ApiOperation(value = "View team members", notes = "Get the list of team members", httpMethod = "GET", response = List.class)
	public ResponseEntity<List<UserModel>> viewTeam() {
		log.info("Inside team function");
		return new ResponseEntity<List<UserModel>>(userService.viewTeam(), HttpStatus.OK);
	}
	
	@GetMapping("/unused")
	public void getallocatedtrainersbyid() {
		if(authorizationClient.validate(null)) {
			
		}
	}

	@PostMapping("/adduser")
	@ApiOperation(value = "Add new user", notes = "Add a user data", httpMethod = "POST", response = List.class)
	public boolean addUser(@Valid @RequestBody UserRequest userRequest) {
		log.info("Executing adduser function");

		UserModel user = new UserModel();
		String userRequestRole = userRequest.getRole();
		Set<RoleModel> roleModel = new HashSet<>();
		RoleModel userRole = new RoleModel();

		if (userRequestRole.equalsIgnoreCase("Trainer")) {
			userRole = roleRepository.findByRole(ERole.Trainer).orElse(null);
		} else if (userRequestRole.equalsIgnoreCase("Hr")) {
			userRole = roleRepository.findByRole(ERole.HR).orElse(null);
		} else if (userRequestRole.equalsIgnoreCase("Manager")) {
			userRole = roleRepository.findByRole(ERole.Manager).orElse(null);
		} else if (userRequestRole.equalsIgnoreCase("Admin")) {
			userRole = roleRepository.findByRole(ERole.Admin).orElse(null);
		}

		roleModel.add(userRole);

		user.setFname(userRequest.getFname());
		user.setLname(userRequest.getLname());
		user.setEmail(userRequest.getEmail());
		user.setStatus(userRequest.getStatus());
		user.setPass(passwordEncoder.encode(userRequest.getPass()));
		user.setGender(userRequest.getGender());
		user.setPhone(userRequest.getPhone());
		user.setSkills(userRequest.getSkills());
		user.setQualification(userRequest.getQualification());
		user.setExperience(userRequest.getExperience());
		user.setCapacity(userRequest.getCapacity());

		String address = userRequest.getHolding() + ", " + userRequest.getArea() + ", " + userRequest.getCity() + ", "
				+ userRequest.getState() + ", " + userRequest.getCountry() + ", " + userRequest.getPin();

		user.setAddress(address);
		user.setCompany(userRequest.getCompany());
		user.setRole(roleModel);
		return userService.save(user);

	}

	@PutMapping("/user/{id}")
	@ApiOperation(value = "Update a user", notes = "Update the datas of user", httpMethod = "PUT", response = UserModel.class)
	public ResponseEntity<Boolean> updateUser(@PathVariable(name = "id") int id, @RequestBody UserRequest userRequest) {
		log.info("Inside the Update function");
		return new ResponseEntity<Boolean>(userService.updateUser(id, userRequest), HttpStatus.OK);
	}

	@DeleteMapping("/user/{id}")
	@ApiOperation(value = "Delete a user", notes = "Delete the data of user by id", httpMethod = "DELETE", response = UserModel.class)
	public ResponseEntity<Boolean> removeUser(@PathVariable(name = "id") int id) {
		log.info("Executing the Delete function");
		return new ResponseEntity<Boolean>(userService.removeUser(id), HttpStatus.OK);
	}

	@PutMapping("/user/{id}/{newstatus}")
	@ApiOperation(value = "Update user's status", notes = "Update the status of user by his id", httpMethod = "PUT", response = UserModel.class)
	public ResponseEntity<Boolean> updateUserStatus(@PathVariable(name = "id") int id,
			@PathVariable(name = "newstatus") String newStatus) {
		log.info("Inside update user function");
		return new ResponseEntity<Boolean>(userService.updateUserStatus(id, newStatus), HttpStatus.OK);
	}

	@GetMapping("/availabletrainers/{code}")
	@ApiOperation(value = "Get available trainers", notes = "Get the list of the available trainers", httpMethod = "GET", response = UserModel.class)
	public ResponseEntity<List<UserModel>> getAvailableTrainers(@PathVariable(name = "code") String code) {
		log.info("Inside availabletrainer function");
		return new ResponseEntity<List<UserModel>>(userService.viewAvailableTrainers(code), HttpStatus.OK);
	}

	@PutMapping("/user/{id}/capacity/{newcapacity}")
	@ApiOperation(value = "Update user's capacity", notes = "Update the capacity of user by his id and capacity amount", httpMethod = "PUT", response = UserModel.class)
	public ResponseEntity<Boolean> updateUserCapacity(@PathVariable(name = "id") int id,
			@PathVariable(name = "newcapacity") String newCapacity) {
		log.info("Executing update function");
		return new ResponseEntity<Boolean>(userService.updateUserStatus(id, newCapacity), HttpStatus.OK);
	}

	@GetMapping("/users/{email}")
	public ResponseEntity<?> getUsersByEmail(@PathVariable(name = "email") String email, @CookieValue(name = "tms") String token) {
		log.info("Inside getuser function");
			
		if(authorizationClient.validate(token)) {
			UserModel user = new UserModel();
			user = userService.viewUserByEmail(email);
			if (user == null) {
				throw new UserNotFoundException("User Not Found");
			}
			else return new ResponseEntity<UserModel>(userService.viewUserByEmail(email), HttpStatus.OK);
		}
		else {
			return ResponseEntity.badRequest().body("Login Required");
		}
		
		
		
	}
}
