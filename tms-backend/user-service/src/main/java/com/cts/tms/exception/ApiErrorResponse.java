package com.cts.tms.exception;

import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class is for customizing the exception handler
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiOperation(value = "Api Error Response Class")
public class ApiErrorResponse {

	private HttpStatus httpStatus;
	private String message;


	public ApiErrorResponse(HttpStatus httpStatus) {
		this();
		this.httpStatus = httpStatus;
	}

	
	public ApiErrorResponse(HttpStatus httpStatus, Throwable throwable) {
		this();
		this.httpStatus = httpStatus;
		this.message = "Unexpected error in the request";
	}

}
