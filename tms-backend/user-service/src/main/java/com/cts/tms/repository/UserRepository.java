package com.cts.tms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cts.tms.model.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
//	Approved Trainers
	@Query(
			value = " SELECT * FROM users u " + 
					" INNER JOIN user_roles ur ON u.u_id = ur.u_id " + 
					" INNER JOIN roles r ON r.r_id = ur.r_id " + 
					" WHERE LCASE(r.r_role) = 'trainer' AND LCASE(u.u_status) = 'approved' " + 
					" ORDER BY u.u_id ASC ",
			nativeQuery = true)
	List<UserModel> findApprovedTrainers();

//	Pending Trainers
	@Query(
			value = " SELECT * FROM users u " + 
					" INNER JOIN user_roles ur ON u.u_id = ur.u_id " + 
					" INNER JOIN roles r ON r.r_id = ur.r_id " + 
					" WHERE LCASE(r.r_role) = 'trainer' AND LCASE(u.u_status) = 'pending' " + 
					" ORDER BY u.u_id ASC ", 
			nativeQuery = true)
	List<UserModel> findPendingTrainers();
	
//	Team
	@Query(
			value = " SELECT * FROM users u " + 
					" INNER JOIN user_roles ur ON u.u_id = ur.u_id " + 
					" INNER JOIN roles r ON r.r_id = ur.r_id " + 
					" WHERE LCASE(r.r_role) IN ('hr','manager') ", 
			nativeQuery = true)
	List<UserModel> findTeam();
	
	
//	Get User
	Optional<UserModel> findById(int id);
	
	Optional<UserModel> findByEmail(String email);
	
	@Query(value = " SELECT * FROM users u "
			+ " INNER JOIN user_roles ur ON u.u_id = ur.u_id "
			+ " INNER JOIN roles r ON r.r_id = ur.r_id "
			+ " WHERE LCASE(r.r_role) = 'trainer' AND LCASE(u.u_status) = 'approved' "
			+ " AND u.u_capacity >= (SELECT c.c_strength FROM Cohorts c WHERE c.c_code = ?1)",nativeQuery = true)
	List<UserModel> findAvailableTrainersByCohortCode(String code);
}
