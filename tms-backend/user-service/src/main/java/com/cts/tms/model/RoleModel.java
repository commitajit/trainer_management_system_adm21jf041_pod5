package com.cts.tms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Entity
@Table(name = "roles")
@Data
@Slf4j
public class RoleModel {
  @ApiModelProperty(value = "Id of the RoleModel",required= true , dataType="int")
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "r_id")
  private int id;
  
  @Enumerated(EnumType.STRING)
  @Column(name = "r_role")
  private ERole role;
  
  public RoleModel() {
  }
  
  public RoleModel(ERole role) {
    this.role = role;
  }
}