package com.cts.tms.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

	@ApiModelProperty(value = "Id of the UserModel.",required= true , dataType="int")
	@Id
	@Column(name = "u_id")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;

	@ApiModelProperty(value = "First name of the user",required= true , dataType="String")
	@Column(name = "u_fname")
	@NotNull(message = "first name can not be empty")
	private String fname;

	@ApiModelProperty(value = "Last name of the user",required= true , dataType="String")
	@Column(name = "u_lname")
	@NotNull(message = "last name can not be empty")
	private String lname;

	@ApiModelProperty(value = "Email of the user",required= true , dataType="String")
	@Column(name = "u_email")
	@NotNull(message = "Email can not be empty")
	private String email;

	@ApiModelProperty(value = "Password of the user",required= true , dataType="String")
	@Column(name = "u_pass")
	private String pass;

	@ApiModelProperty(value = "verification status of user",required= true , dataType="String")
	@Column(name = "u_status")
	@NotNull(message = "Status can not be empty")
	private String status;

	@ApiModelProperty(value = "Gender of the user",required= true , dataType="String")
	@Column(name = "u_gender")
	@NotNull(message = "Gender can not be empty")
	private String gender;

	@ApiModelProperty(value = "Phone of the user",required= true , dataType="String")
	@Column(name = "u_phone")
	@NotNull(message = "Phone can not be empty")
	private String phone;

	@ApiModelProperty(value = "Skills of the user",required= true , dataType="String")
	@Column(name = "u_skills")
	private String skills;

	@ApiModelProperty(value = "Qualifications of the user",required= true , dataType="String")
	@Column(name = "u_qualification")
	private String qualification;

	@ApiModelProperty(value = "Years of Experience of the user",required= true , dataType="String")
	@Column(name = "u_experience")
	private int experience;

	@ApiModelProperty(value = "Capacity of the user",required= true , dataType="int")
	@Column(name = "u_capacity")
	private int capacity;

	@ApiModelProperty(value = "Address of the user",required= true , dataType="String")
	@Column(name = "u_address")
	private String address;

	@ApiModelProperty(value = "Company name of the user",required= true , dataType="String")
	@Column(name = "u_company")
	@NotNull(message = "Company can not be empty")
	private String company;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "u_id"), inverseJoinColumns = @JoinColumn(name = "r_id"))
	private Set<RoleModel> role = new HashSet<>();
}
