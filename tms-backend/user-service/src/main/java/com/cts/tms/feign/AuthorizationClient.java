package com.cts.tms.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Proxy interface for authorization service
 */
@FeignClient(name = "auth-service", url = "http://localhost:8091/tms/api/v1")
public interface AuthorizationClient {

	/**
	 * Method for validating the token
	 * 
	 * @param token
	 * @return This returns true if token is valid else returns false
	 */
	@GetMapping("/auth/validate")
	public boolean validate(@CookieValue("tms") String token);
}