package com.cts.tms.exception;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public UserNotFoundException(String message) {
		super(message);
	}

}
