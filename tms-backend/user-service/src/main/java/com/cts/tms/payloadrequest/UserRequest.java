package com.cts.tms.payloadrequest;

import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
public class UserRequest {
	private int id;
	
	private String fname;

	private String lname;
	
	private String email;
	
	private String pass;
	
	private String status;
	
	private String gender;
	
	private String phone;
	
	private String skills;
	
	private String qualification;
	
	private int experience;
	
	private int capacity;
	
	private String address;
	
	private String company;
	
	private String role;
	
	private String holding;
	private String area;
	private String city;
	private String state;
	private String country;
	private String pin;
}
