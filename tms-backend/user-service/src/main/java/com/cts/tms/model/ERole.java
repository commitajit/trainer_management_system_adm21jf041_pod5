package com.cts.tms.model;

public enum ERole {
	  Admin,
	  HR,
	  Manager,
	  Trainer
	}