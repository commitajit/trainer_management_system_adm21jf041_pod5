package com.cts.tms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cts.tms.model.UserModel;
import com.cts.tms.payloadrequest.UserRequest;

@Service
public interface UserService {
	
//	View (Get) Methods
	public List<UserModel> viewApprovedTrainer();
	public List<UserModel> viewPendingTrainer();
	public List<UserModel> viewTeam();
	public List<UserModel> viewAvailableTrainers(String cohortCode);
	
	public UserModel viewUserById(int id); 
	public UserModel viewUserByEmail(String email);
	
//	Save (POST) Methods
	public boolean save(UserModel newUser);
	
//	Update (PUT) Methods
	public boolean updateUser(int id, UserRequest userRequest);
	public boolean updateUserStatus(int id, String newStatus);
	public boolean updateUserCapacity(int id, String newCapacity);
	
//	Delete (Delete) Methods
	public boolean removeUser(int id);
	
	// Exception Handling
	public UserModel getUserDetails(int id);
	
}
