package com.cts.tms.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cts.tms.exception.UserNotFoundException;
import com.cts.tms.exception.UserNotSavedException;
import com.cts.tms.model.ERole;
import com.cts.tms.model.RoleModel;
import com.cts.tms.model.UserModel;
import com.cts.tms.payloadrequest.UserRequest;
import com.cts.tms.repository.RoleRepository;
import com.cts.tms.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public List<UserModel> viewApprovedTrainer() {
		return userRepository.findApprovedTrainers();
	}

	@Override
	public List<UserModel> viewPendingTrainer() {
		return userRepository.findPendingTrainers();
	}

	@Override
	public List<UserModel> viewTeam() {
		List<UserModel> existingUsers = userRepository.findTeam();
		if (existingUsers != null) {
			return userRepository.findTeam();
		} else
			throw new UserNotFoundException("User dose not exists!");
	}

	@Override
	public boolean save(UserModel newUser) {
		try {
			userRepository.save(newUser);
			return true;
		} catch (Exception e) {
			throw new UserNotSavedException("User data not saved!");
		}

	}

	@Override
	public boolean updateUser(int id, UserRequest userRequest) {
		boolean resultStatus;
		try {
			UserModel existingUser = new UserModel();
			existingUser = userRepository.findById(id).orElseThrow(null);
			UserModel user = new UserModel();
			String userRequestRole = userRequest.getRole();
			Set<RoleModel> roleModel = new HashSet<>();
			RoleModel userRole = new RoleModel();

			if (userRequest.getFname().trim().length() != 0) {
				existingUser.setFname(userRequest.getFname());
			}
			if (userRequest.getLname().trim().length() != 0) {
				existingUser.setLname(userRequest.getLname());
			}
			if (userRequest.getEmail().trim().length() != 0) {
				existingUser.setEmail(userRequest.getEmail());
			}
			if (userRequest.getStatus().trim().length() != 0) {
				existingUser.setStatus(userRequest.getStatus());
			}
			if (userRequest.getGender().trim().length() != 0) {
				existingUser.setGender(userRequest.getGender());
			}
			if (userRequest.getPhone().trim().length() != 0) {
				existingUser.setPhone(userRequest.getPhone());
			}
			if (userRequest.getSkills().trim().length() != 0) {
				existingUser.setSkills(userRequest.getSkills());
			}
			if (userRequest.getQualification().trim().length() != 0) {
				existingUser.setQualification(userRequest.getQualification());
			}
			if (String.valueOf(userRequest.getExperience()).length() != 0) {
				existingUser.setExperience(userRequest.getExperience());
			}
			if (String.valueOf(userRequest.getCapacity()).length() != 0) {
				existingUser.setCapacity(userRequest.getCapacity());
			}
			if (String.valueOf(userRequest.getCompany()).length() != 0) {
				existingUser.setCompany(userRequest.getCompany());
			}

//			Role Update
			if (userRequest.getRole().trim().length() != 0) {
				if (userRequestRole.equalsIgnoreCase("Hr")) {
					userRole = roleRepository.findByRole(ERole.HR).orElse(null);
				} else if (userRequestRole.equalsIgnoreCase("Manager")) {
					userRole = roleRepository.findByRole(ERole.Manager).orElse(null);
				}
				roleModel.add(userRole);
				existingUser.setRole(roleModel);
			}

//			Set Address
			String[] arrAddress = new String[6];
			arrAddress = existingUser.getAddress().split(", ");

			if (userRequest.getHolding().trim().length() != 0) {
				arrAddress[0] = userRequest.getHolding();
			}
			if (userRequest.getArea().trim().length() != 0) {
				arrAddress[1] = userRequest.getArea();
			}
			if (userRequest.getCity().trim().length() != 0) {
				arrAddress[2] = userRequest.getCity();
			}
			if (userRequest.getState().trim().length() != 0) {
				arrAddress[3] = userRequest.getState();
			}
			if (userRequest.getCountry().trim().length() != 0) {
				arrAddress[4] = userRequest.getCountry();
			}
			if (userRequest.getPin().trim().length() != 0) {
				arrAddress[5] = userRequest.getPin();
			}

			String address = null;

			address = arrAddress[0] + ", " + 
						arrAddress[1] + ", " + 
						arrAddress[2] + ", " + 
						arrAddress[3] + ", " + 
						arrAddress[4] + ", " + 
						arrAddress[5];
			
			existingUser.setAddress(address);

			userRepository.save(existingUser);
			resultStatus = true;

		} catch (Exception e) {
			resultStatus = false;
		}

		return resultStatus;
	}

	@Override
	public boolean removeUser(int id) {
		UserModel user = userRepository.findById(id).orElse(null);
		if(user != null) {
			userRepository.delete(user);
			return true;
		}
		else throw new UserNotFoundException("User does not exists!");
		
		
	}

	@Override
	public UserModel viewUserById(int id) {
		return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User does not exists!"));
	}

	@Override
	public UserModel viewUserByEmail(String email) {
		UserModel user = new UserModel();
		try {
			user = userRepository.findByEmail(email).orElseThrow(null);
		} catch (Exception e) {
			user = null;
		}
		return user;
	}

	@Override
	public boolean updateUserStatus(int id, String newStatus) {
		boolean resultStatus;
		try {
			UserModel existingUser = userRepository.findById(id).orElseThrow(null);
			existingUser.setStatus(newStatus);
			userRepository.save(existingUser);
			resultStatus = true;
		} catch (Exception E) {
			resultStatus = false;
		}
		return resultStatus;
	}

	@Override
	public List<UserModel> viewAvailableTrainers(String cohortCode) {
		List<UserModel> existingUser = userRepository.findAvailableTrainersByCohortCode(cohortCode);
		if (existingUser != null) {
			return existingUser;
		} else
			throw new UserNotFoundException("Cannot find the collateral loan with id:");
	}

	@Override
	public boolean updateUserCapacity(int id, String newCapacity) {
		boolean resultStatus;
		try {
			UserModel existingUser = userRepository.findById(id).orElseThrow(null);
			existingUser.setCapacity(Integer.parseInt(newCapacity));
			userRepository.save(existingUser);
			resultStatus = true;
		} catch (Exception E) {
			resultStatus = false;
		}
		return resultStatus;
	}

	@Override
	public UserModel getUserDetails(int id) {
		UserModel user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User not found!!!"));
		return user;
	}
}
