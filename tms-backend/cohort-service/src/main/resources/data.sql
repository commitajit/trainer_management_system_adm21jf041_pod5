/* ------------  USERS TABLE  ------------*/
CREATE TABLE users (
	u_id INT AUTO_INCREMENT NOT NULL,
	u_fname VARCHAR(25) NOT NULL,
	u_lname VARCHAR(25) NOT NULL,
	u_email VARCHAR(50) NOT NULL,
	u_password VARCHAR(255),
	u_gender VARCHAR(10) NOT NULL,
	u_phone VARCHAR(10) NOT NULL,
	u_skills VARCHAR(100) NOT NULL,
	u_qualification VARCHAR(50) NOT NULL,
	u_experience INT NOT NULL,
	u_address VARCHAR(100) NOT NULL,
	u_company VARCHAR(100) NOT NULL,
	PRIMARY KEY (u_id)
);

/* ------------  COHORTS TABLE  ------------*/
CREATE TABLE cohorts (
	c_code VARCHAR(25) NOT NULL,
	c_strength INT NOT NULL,
	c_domain VARCHAR(50) NOT NULL,
	u_id INT,
	PRIMARY KEY (c_code),
	FOREIGN KEY (u_id) REFERENCES users(u_id)
);