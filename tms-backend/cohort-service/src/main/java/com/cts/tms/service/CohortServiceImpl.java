package com.cts.tms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cts.tms.model.CohortModel;
import com.cts.tms.repository.CohortRepository;

@Service
public class CohortServiceImpl implements CohortService {

	@Autowired
	private CohortRepository cohortRepository;

	public CohortServiceImpl(CohortRepository cohortRepository) {
		this.cohortRepository = cohortRepository;
	}

	@Override
	public List<CohortModel> viewCohorts() {
		// TODO Auto-generated method stub
		return cohortRepository.findCohorts();
	}

	@Override
	public CohortModel saveCohort(CohortModel newCohort) {
		return cohortRepository.save(newCohort);
	}

	@Override
	public boolean deleteCohort(String code) {

		CohortModel cohort = cohortRepository.findCohortByCode(code);

		try {

			cohortRepository.delete(cohort);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean updateCohort(String code, CohortModel cohortDetails) {

		CohortModel cohort = cohortRepository.findCohortByCode(code);

		try {
			if (cohortDetails.getCode().trim().length() != 0) {
				cohort.setCode(cohortDetails.getCode());
			}
			if (cohortDetails.getDomain().trim().length() != 0) {
				cohort.setDomain(cohortDetails.getDomain());
			}
			if (cohortDetails.getStrength() != 0) {
				cohort.setStrength(cohortDetails.getStrength());
			}
			if (String.valueOf(cohortDetails.getUserId()).trim().length() != 0) {
				cohort.setUserId(cohortDetails.getUserId());
			}
			
			cohortRepository.save(cohort);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	// Exception handling
	@Override
	public CohortModel getCohortDetails(String code) {
		CohortModel cohort = cohortRepository.findCohortByCode(code);
		return cohort;
	}

	@Override
	public List<CohortModel> viewUnallocatedCohorts() {
		return cohortRepository.findUnallocatedCohorts();
	}

	@Override
	public List<String> viewAllocatedCohorts() {
		return cohortRepository.findAllocatedCohorts();
	}

	@Override
	public boolean unallocateCohort(String code) {
		boolean resultStatus = false;
		try {
			CohortModel cohort = cohortRepository.findCohortByCode(code);
			cohort.setUserId(null);
			cohortRepository.save(cohort);
			resultStatus = true;
		} catch(Exception e) {
			resultStatus = false;
		}
		return resultStatus;
	}
	
}
