package com.cts.tms.repository;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cts.tms.model.CohortModel;

@Repository
public interface CohortRepository extends JpaRepository<CohortModel, String>{
	
	@Query(value = "SELECT * FROM cohorts c WHERE c.u_id IS NULL ", nativeQuery = true)
	List<CohortModel> findUnallocatedCohorts();
	
	@Query(value = "SELECT c.c_code, c.c_strength, c.c_domain, u.u_id, u.u_fname, u.u_lname, u.u_email	" + 
				   " FROM cohorts c INNER JOIN users u ON u.u_id = c.u_id", nativeQuery = true)
	List<String> findAllocatedCohorts();
	
	@Query(value = "SELECT * FROM cohorts c",nativeQuery = true)
	List<CohortModel> findCohorts();

	@Query(value = "SELECT * FROM cohorts c WHERE c.c_code = ?1",nativeQuery = true)
	CohortModel findCohortByCode(String code);
	
	//jUnit
	public Boolean existsByCode(String code);
	
	
}
