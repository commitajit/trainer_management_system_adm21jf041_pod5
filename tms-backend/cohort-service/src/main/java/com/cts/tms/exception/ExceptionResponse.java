package com.cts.tms.exception;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ApiOperation(value="Error response class")
public class ExceptionResponse {
    private String message;
    private LocalDateTime dateTime;
      
}
