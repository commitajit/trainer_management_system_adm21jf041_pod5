package com.cts.tms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CohortServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CohortServiceApplication.class, args);
	}
	
}
