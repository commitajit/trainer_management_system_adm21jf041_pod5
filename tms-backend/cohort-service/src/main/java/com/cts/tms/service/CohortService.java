package com.cts.tms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cts.tms.model.CohortModel;

@Service
public interface CohortService {
	public List<CohortModel> viewCohorts();
	public List<CohortModel> viewUnallocatedCohorts();
	public List<String> viewAllocatedCohorts();
	public CohortModel saveCohort(CohortModel newCohort);
	public boolean deleteCohort(String code);
	public boolean updateCohort(String code, CohortModel cohortDetails);
	public boolean unallocateCohort(String code);
	
	//exception handling
	public CohortModel getCohortDetails(String code);
	
}    

