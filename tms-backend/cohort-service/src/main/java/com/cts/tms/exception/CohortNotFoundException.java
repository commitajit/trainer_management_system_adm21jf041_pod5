package com.cts.tms.exception;

public class CohortNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public CohortNotFoundException(String message) {
		super(message);
	}
}