package com.cts.tms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.tms.exception.CohortNotFoundException;
import com.cts.tms.model.CohortModel;
import com.cts.tms.service.CohortService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping(value = "tms/api/v1/")
public class CohortController {

	@Autowired
	private CohortService cohortService;

	@GetMapping("/unallocatedcohorts")
	@ApiOperation(value = "Get unallocated cohorts", notes = "Get the list of unallocated cohorts", httpMethod = "GET", response = List.class)
	public ResponseEntity<List<CohortModel>> viewUnallocatedCohorts() {
		log.info("Inside cohort details function");
		return new ResponseEntity<>(cohortService.viewUnallocatedCohorts(), HttpStatus.OK);
	}

	@GetMapping("/allocatedcohorts")
	@ApiOperation(value = "Get allocated cohorts", notes = "Get the list of allocated cohorts", httpMethod = "GET", response = List.class)
	public ResponseEntity<List<String>> viewAllocatedCohorts() {
		log.info("Inside cohort details function");
		return new ResponseEntity<>(cohortService.viewAllocatedCohorts(), HttpStatus.OK);
	}

	@PostMapping("/addcohort")
	@ApiOperation(value = "Add new cohort", notes = "Inserting a new cohort data", httpMethod = "POST", response = List.class)
	public boolean addcohorts(@RequestBody CohortModel newCohort) {
		log.info("Inside add cohorts function");
		CohortModel add = new CohortModel();

		add.setCode(newCohort.getCode());
		add.setDomain(newCohort.getDomain());
		add.setStrength(newCohort.getStrength());

		try {
			cohortService.saveCohort(add);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@DeleteMapping(value = "/cohortsdetails/{code}")
	@ApiOperation(value = "Delete a cohort", notes = "Deleting the cohort data", httpMethod = "DELETE", response = List.class)
	public ResponseEntity<Boolean> deletedetails(@PathVariable String code) {
		log.info("Executing the delete function");
		return new ResponseEntity<>(cohortService.deleteCohort(code), HttpStatus.OK);
	}

	@PutMapping("/cohortsdetails/{code}")
	@ApiOperation(value = "Update the cohort", notes = "Updating the cohort data", httpMethod = "POST", response = List.class)
	public ResponseEntity<Boolean> updateCohort(@PathVariable(value = "code") String code,
			@RequestBody CohortModel cohortDetails) {
		log.info("Executing the update function");
		return new ResponseEntity<Boolean>(cohortService.updateCohort(code, cohortDetails), HttpStatus.OK);
	}

	@PutMapping("/unallocatecohort/{code}")
	@ApiOperation(value = "Unallocate the cohort", notes = "Unallocate the the cohort with trainer", httpMethod = "POST", response = List.class)
	public ResponseEntity<Boolean> updateCohort(@PathVariable(value = "code") String code) {
		log.info("Executing the update function");
		return new ResponseEntity<Boolean>(cohortService.unallocateCohort(code), HttpStatus.OK);
	}

	@GetMapping("/cohortsdetails/{code}")
	@ApiOperation(value = "Get a cohort", notes = "Get the cohort by code", httpMethod = "POST", response = List.class)
	public ResponseEntity<CohortModel> getCohortDetails(@PathVariable("code") String code) {
		CohortModel cohort = cohortService.getCohortDetails(code);
		if (cohort == null)
			throw new CohortNotFoundException("Cohort Not Found!!!");
		return new ResponseEntity<CohortModel>(cohort, HttpStatus.OK);
	}

}
