package com.cts.tms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cohorts")
public class CohortModel {
	
	@ApiModelProperty(value = "Code of the cohort",required= true , dataType="String")
	@Id
	@Column(name = "c_code")
	private String code;
	
	@ApiModelProperty(value = "Strength of the Cohort",required= true , dataType="int")
	@Column(name = "c_strength")
	private int strength;
	
	@ApiModelProperty(value = "Domain of the Cohort",required= true , dataType="String")
	@Column(name = "c_domain")
	private String domain;
	
	@ApiModelProperty(value = "user id of the approved trainer.",required= true , dataType="String")
	@Column(name = "u_id")
	private String userId;

}
