package com.cts.tms.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;
import com.cts.tms.model.CohortModel;
import com.cts.tms.repository.CohortRepository;

@SpringBootTest
class CohortRepoTest {
 
    @Autowired
    private CohortRepository cohortRepository;
 
    @Test
    void existsByCode() {
        CohortModel cohort = new CohortModel("ADM21JF055", 27,"ADM", null);
        cohortRepository.save(cohort);
        Boolean actualResult = cohortRepository.existsByCode("ADM21JF055");
        assertThat(actualResult).isTrue();
    }
}