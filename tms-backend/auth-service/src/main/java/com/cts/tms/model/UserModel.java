package com.cts.tms.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

	@ApiModelProperty(value = "Id of the UserModel.")
	@Id
	@Column(name = "u_id")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;

	@ApiModelProperty(value = "First name of the user")
	@Column(name = "u_fname")
	private String fname;

	@ApiModelProperty(value = "Last name of the user")
	@Column(name = "u_lname")
	private String lname;

	@ApiModelProperty(value = "Email of the user")
	@Column(name = "u_email")
	private String email;

	@ApiModelProperty(value = "Password of the user")
	@Column(name = "u_pass")
	private String pass;

	@ApiModelProperty(value = "verification status of user")
	@Column(name = "u_status")
	private String status;

	@ApiModelProperty(value = "Gender of the user")
	@Column(name = "u_gender")
	private String gender;

	@ApiModelProperty(value = "Phone of the user")
	@Column(name = "u_phone")
	private String phone;

	@ApiModelProperty(value = "Skills of the user")
	@Column(name = "u_skills")
	private String skills;

	@ApiModelProperty(value = "Qualifications of the user")
	@Column(name = "u_qualification")
	private String qualification;

	@ApiModelProperty(value = "Years of Experience of the user")
	@Column(name = "u_experience")
	private int experience;

	@ApiModelProperty(value = "Capacity of the user")
	@Column(name = "u_capacity")
	private int capacity;

	@ApiModelProperty(value = "Address of the user")
	@Column(name = "u_address")
	private String address;

	@ApiModelProperty(value = "Company name of the user")
	@Column(name = "u_company")
	private String company;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "u_id"), inverseJoinColumns = @JoinColumn(name = "r_id"))
	private Set<RoleModel> role = new HashSet<>();

}
