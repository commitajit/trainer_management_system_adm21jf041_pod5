package com.cts.tms.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cts.tms.model.ERole;
import com.cts.tms.model.RoleModel;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Integer>{
	Optional<RoleModel> findByRole(ERole role);
}
