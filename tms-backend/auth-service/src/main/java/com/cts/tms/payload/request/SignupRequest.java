package com.cts.tms.payload.request;

import javax.validation.constraints.*;

import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
public class SignupRequest {
	
	private int id;
	
	@NotBlank
	private String fname;

	@NotBlank
	private String lname;
	
	@NotBlank
	private String email;
	
	private String pass;
	
	@NotBlank
	private String status;
	
	@NotBlank
	private String gender;
	
	@NotBlank
	private String phone;
	
	@NotBlank
	private String skills;
	
	@NotBlank
	private String qualification;
	
	@NotBlank
	private int experience;
	
	@NotBlank
	private int capacity;
	
	@NotBlank
	private String address;
	
	@NotBlank
	private String company;
	
	@NotBlank
	private String role;    
}
