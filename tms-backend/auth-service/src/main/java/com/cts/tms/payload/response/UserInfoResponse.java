package com.cts.tms.payload.response;

import java.util.List;

import lombok.Data;

@Data
public class UserInfoResponse {
	private int id;
	private String email;
	private List<String> roles;

	public UserInfoResponse(int id, String email, List<String> roles) {
		this.id = id;
		this.email = email;
		this.roles = roles;
	}
}