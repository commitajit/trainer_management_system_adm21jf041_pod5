package com.cts.tms.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cts.tms.model.ERole;
import com.cts.tms.model.RoleModel;
import com.cts.tms.model.UserModel;
import com.cts.tms.payload.request.LoginRequest;
import com.cts.tms.payload.request.SignupRequest;
import com.cts.tms.payload.response.MessageResponse;
import com.cts.tms.payload.response.UserInfoResponse;
import com.cts.tms.repository.RoleRepository;
import com.cts.tms.repository.UserRepository;
import com.cts.tms.security.jwt.JwtUtils;
import com.cts.tms.security.services.UserDetailsImpl;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@RequestMapping("/tms/api/v1")
@Slf4j
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@ApiOperation(value = "Login purpose only", notes = "Endpoint requires email & password", httpMethod = "POST", response = UserInfoResponse.class)
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		log.debug("Logging process Started");
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPass()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		log.debug("Getting User's name");
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		
		log.debug("Generating JWT token");
		ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString()).body(
				new UserInfoResponse(userDetails.getId(), userDetails.getEmail(), roles));
	}

	@PostMapping("/register")
	@ApiOperation(value = "Register purpose only", notes = "Endpoint requires email, password & company", httpMethod = "POST", response = UserInfoResponse.class)
	public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already taken!"));
		} else {
			UserModel user = new UserModel();
			String signupRequestRole = signUpRequest.getRole();
			Set<RoleModel> roleModel = new HashSet<>();
			RoleModel userRole = new RoleModel();

			if (signupRequestRole == null) {
				userRole = roleRepository.findByRole(ERole.Admin)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found in Request."));

			} else if (signupRequestRole.equalsIgnoreCase("Admin")) {
				userRole = roleRepository.findByRole(ERole.Admin).orElse(null);
			}

			roleModel.add(userRole);

			user.setFname("Admin");
			user.setLname("Admin");
			user.setEmail(signUpRequest.getEmail());
			user.setPass(encoder.encode(signUpRequest.getPass()));
			user.setStatus("Approved");
			user.setGender("Admin");
			user.setPhone("Admin");
			user.setSkills("Admin");
			user.setQualification("Admin");
			user.setExperience(0);
			user.setCapacity(0);
			user.setAddress("Admin");
			user.setCompany(signUpRequest.getCompany());
			user.setRole(roleModel);

			userRepository.save(user);
			return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
		}

	}

	@PostMapping("/logout")
	public ResponseEntity<?> logoutUser() {
		ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
				.body(new MessageResponse("You've been signed out!"));
	}

	@GetMapping("/auth/validate")
	public boolean validate(@CookieValue("tms") String token) {
		return jwtUtils.validateJwtToken(token);
	}

}