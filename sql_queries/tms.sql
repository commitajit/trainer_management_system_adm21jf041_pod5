/* ------------  DROPINGS  ------------*/
DROP SCHEMA tms;
DROP TABLE users;
DROP TABLE rolesg;
DROP TABLE user_roles;
DROP TABLE cohorts;
/* ------------------------------------------*/


/* ------------  INITIALIZATION  ------------*/
CREATE SCHEMA tms;
USE tms;
/* ------------------------------------------*/



/* ------------  USERS TABLE BEGINS  ------------*/
CREATE TABLE users (
	u_id INT AUTO_INCREMENT,				-- Primary Key (int)
	u_fname VARCHAR(25) NOT NULL,			-- First Name
	u_lname VARCHAR(25) NOT NULL,			-- Last Name
	u_email VARCHAR(80) NOT NULL,			-- Email
	u_pass VARCHAR(255),					-- Password
    u_status VARCHAR(25) NOT NULL,			-- Verification Status (Yes / No)
	u_gender VARCHAR(10) NOT NULL,			-- Gender (Male / Female)
	u_phone VARCHAR(10) NOT NULL,			-- Phone Number
	u_skills VARCHAR(100) NOT NULL,			-- Skills (Skill1, Skill2, Skill3)
	u_qualification VARCHAR(50) NOT NULL,	-- Highest Qualification (B.Sc)
	u_experience INT NOT NULL,				-- Years of Experience
    u_capacity INT,							-- Capacity (For Trainers)
	u_address VARCHAR(100) NOT NULL,		-- Full Address (House No, Street, City, State, Country)
	u_company VARCHAR(100) NOT NULL,		-- Comapny Name
	PRIMARY KEY (u_id)						
);

-- INSERT ADMIN
INSERT INTO users (u_fname, u_lname, u_email, u_pass, u_status, u_gender, u_phone, u_skills, u_qualification, u_experience, u_capacity, u_address, u_company) VALUES
("Ajit","Singh", "admin@cognizant.com", "pass@111", "YES", "NA", "NA", "NA", "NA", 0, 0, "NA", "Cognizant");

-- INSERT Trainers
INSERT INTO users (u_fname, u_lname, u_email, u_status, u_gender, u_phone, u_skills, u_qualification, u_experience, u_capacity, u_address, u_company) VALUES
("Priya","Kumari", "priya@cognizant.com", "NO", "Female", "8901234567", "Excel, Word, PowerPoint", "M.Sc", 2, "90", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant"),
("Mira","Kumari", "mira@cognizant.com", "NO", "Female", "8901234567", "Excel, Word, PowerPoint", "B.Sc", 2, "90", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant"),
("Deepak","Atmaprakash", "deepatm@cognizant.com", "NO", "Male", "8734934713", "Excel, Word, PowerPoint", "M.Ca", 2, "90", "12, Manaitand, Gaya, Bihar, India", "Cognizant"),
("Rohit","Anilkumar", "Rohit@cognizant.com", "NO", "Male", "9991234567", "Word", "I.Sc", 2, "90", "12, Manaitand, Kaipoda, Kerla, India", "Cognizant"),
("Sanjana","Kumari", "Sanajan@cognizant.com", "NO", "Female", "8955234567", "Excel, Word, PowerPoint", "B.Tech", 2, "90", "12, Manaitand, Kolkata, Bengal, India", "Cognizant");

-- INSERT Manager
INSERT INTO users (u_fname, u_lname, u_email, u_pass, u_status, u_gender, u_phone, u_skills, u_qualification, u_experience, u_capacity, u_address, u_company) VALUES
("Nehal","Kumar", "priya@cognizant.com", "pass@333", "NO", "Male", "8901554567", "Excel, Word, PowerPoint", "M.Sc", 2, "0", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant"),
("Nayadeep","Biruli", "mira@cognizant.com", "pass@333", "NO", "Female", "8901436567", "Excel, Word, PowerPoint", "B.Sc", 2, "0", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant");

-- INSERT HR
INSERT INTO users (u_fname, u_lname, u_email, u_pass, u_status, u_gender, u_phone, u_skills, u_qualification, u_experience, u_capacity, u_address, u_company) VALUES
("Pooja","Kumari", "priya@cognizant.com", "pass@222", "NO", "Female", "8901234098", "Excel, Word, PowerPoint", "M.Sc", 2, "0", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant"),
("Piyush","Mohanty", "mira@cognizant.com", "pass@222", "NO", "Female", "9991256767", "Excel, Word, PowerPoint", "B.Sc", 2, "0", "12, Manaitand, Dhanbad, Jharkhand, India", "Cognizant");


/* ------------------------------------------*/



/* ------------  ROLES TABLE BEGINS  ------------*/
CREATE TABLE roles (
	r_id INT AUTO_INCREMENT NOT NULL,
	r_role VARCHAR(25) NOT NULL,
	PRIMARY KEY (r_id)
);

INSERT INTO roles (r_role) VALUES
("Admin"), 			-- 01. Administrator 
("HR"),				-- 02. Human Resource
("Manager"),		-- 03. Manager
("Trainer");		-- 04. Trainer

select * from roles;
/* ------------------------------------------*/



/* ------------  COHORTS TABLE BEGINS  ------------*/
CREATE TABLE cohorts (
	c_code VARCHAR(25) NOT NULL,
	c_strength INT NOT NULL,
	c_domain VARCHAR(50) NOT NULL,
	u_id INT,
	PRIMARY KEY (c_code),
	FOREIGN KEY (u_id) REFERENCES users(u_id)
);

INSERT INTO cohorts VALUES("ADM21JF046", "30", "ADM", null);
INSERT INTO cohorts VALUES("ADM21JF047", "100", "ADM", null);
INSERT INTO cohorts VALUES("ADM21JF048", "110", "ADM", null);

select * from cohorts;
/* ------------------------------------------*/



/* ------------  USER_ROLES TABLE BEGINS  ------------*/
CREATE TABLE user_roles (
	u_id INT NOT NULL,
	r_id INT NOT NULL,
	FOREIGN KEY (u_id) REFERENCES users(u_id),
    FOREIGN KEY (r_id) REFERENCES roles(r_id)
);

SELECT * FROM user_roles;
/* ------------------------------------------*/





/* ------------  ALTERS  ------------*/
ALTER TABLE users;
TRUNCATE TABLE users;
TRUNCATE TABLE user_roles;
TRUNCATE TABLE cohorts;
/* ------------------------------------------*/

/* ------------  SELECTS  ------------*/
SELECT * FROM users;
SELECT * FROM roles;
SELECT * FROM user_roles;
SELECT * FROM cohorts;

/* ------------------------------------------*/


SELECT * FROM users u 
INNER JOIN user_roles ur ON u.u_id = ur.u_id
INNER JOIN roles r ON r.r_id = ur.r_id
WHERE LCASE(r.r_role) = 'trainer' AND LCASE(u.u_status) = 'approved'
AND u.u_capacity >= (SELECT c.c_strength FROM Cohorts c WHERE c.c_code = 'ADM21JF042');

SELECT * FROM cohorts c WHERE c.u_id <> '';
SELECT * FROM cohorts c WHERE c.u_id >= 0;
SELECT * FROM cohorts c WHERE c.u_id IS NULL;